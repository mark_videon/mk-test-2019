﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningSoldier : StateMachineBehaviour
{
    // Reference to the joystick associated with moving
    Joystick moveStick;
    Joystick fireStick;

    float moveRate = 10f;

    // Vector used to store the move stick vector values along appropriate world
    // space co-ordinates
    Vector3 cameraTransformedInputDirection;
    InputToCameraSpace cameraTransformScriptReference;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        moveStick = GameObject.Find("L-Joystick").GetComponent<Joystick>();
        fireStick = GameObject.Find("R-Joystick").GetComponent<Joystick>();

        cameraTransformScriptReference = animator.GetComponent<InputToCameraSpace>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        // Check if the player is attempting to fire while moving, first priority
        if (!Mathf.Approximately(fireStick.Direction.x, 0f) ||
            !Mathf.Approximately(fireStick.Direction.y, 0f))
        {
            cameraTransformedInputDirection = cameraTransformScriptReference.getInputToCameraSpace(fireStick.Direction.x, fireStick.Direction.y);
            animator.gameObject.transform.rotation = Quaternion.LookRotation(cameraTransformedInputDirection, Vector3.up);

            animator.SetBool("isRunning", false);
            animator.SetBool("isShooting", true);
        }
        else if (!Mathf.Approximately(moveStick.Direction.x, 0f) ||
                 !Mathf.Approximately(moveStick.Direction.y, 0f))
        {
            // Movement
            cameraTransformedInputDirection = cameraTransformScriptReference.getInputToCameraSpace(moveStick.Direction.x, moveStick.Direction.y);
            animator.gameObject.transform.position += moveRate * Time.deltaTime * cameraTransformedInputDirection;

            animator.SetBool("isRunning", true);
            animator.gameObject.transform.rotation = Quaternion.LookRotation(cameraTransformedInputDirection, Vector3.up);
        }
        else
        {
            animator.SetBool("isShooting", false);
            animator.SetBool("isRunning", false);
        }

        //Error logging
        if (!moveStick)
        {
            Debug.Log("Could not find left joystick in Running Soldier!");
        }

        if(!fireStick)
        {
            Debug.Log("Could not find right joystick in Running Soldier!");
        }

        if (!cameraTransformScriptReference)
        {
            Debug.Log("Couldn't find the input transformation script in Shooting!");
        }

    } // OnStateUpdate

}
