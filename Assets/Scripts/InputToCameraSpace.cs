﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
    Written under the assumption that movement in x-z plane is of interest.

    Attach to a reference player Gameobject in the scene e.g. player Character.
      
    Relies on the Joystick third-party asset but decoupling should be 
    straightforward.
*/


public class InputToCameraSpace : MonoBehaviour
{

    private Camera referenceCamera;

    private Vector3 pointAboveChar = new Vector3(0f, 0f, 0f);
    private Vector3 pointRightOfChar = new Vector3(0f, 0f, 0f);
    private Vector3 characterScreenPosition = new Vector3(0f, 0f, 0f);

    private Vector3 transformedHorizontal = new Vector3(0f, 0f, 0f);
    private Vector3 transformedVertical = new Vector3(0f, 0f, 0f);
    private Vector3 transformedResultant = new Vector3(0f, 0f, 0f);

    // Start is called before the first frame update
    void Start()
    {
        referenceCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        characterScreenPosition = Camera.main.WorldToScreenPoint(this.transform.position);

        // Screen space, point to the right of char at screen edge
        pointRightOfChar.x = Camera.main.pixelWidth;
        pointRightOfChar.y = characterScreenPosition.y;
        pointRightOfChar.z = characterScreenPosition.z;

        // World space conversion
        pointRightOfChar = Camera.main.ScreenToWorldPoint(pointRightOfChar);
        pointRightOfChar.y = this.transform.position.y; // Movement in world space y undesirable

        transformedHorizontal = pointRightOfChar - this.transform.position;
        transformedHorizontal.Normalize();

        // Screen space 
        pointAboveChar.x = characterScreenPosition.x;
        pointAboveChar.y = Camera.main.pixelHeight;
        pointAboveChar.z = characterScreenPosition.z;

        // World space conversion
        pointAboveChar = Camera.main.ScreenToWorldPoint(pointAboveChar);
        pointAboveChar.y = this.transform.position.y; // Movement in world space y undesirable

        transformedVertical = pointAboveChar - this.transform.position;
        transformedVertical.Normalize();



    }

    public Vector3 getInputToCameraSpace (float horizontalMagnitude, float verticalMagnitude )
    {
        transformedResultant = transformedHorizontal * horizontalMagnitude +
                               transformedVertical * verticalMagnitude;

        return transformedResultant;
    }
}
