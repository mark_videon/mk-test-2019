﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleSoldier : StateMachineBehaviour
{
    Joystick moveStick;
    Joystick fireStick;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        moveStick = GameObject.Find("L-Joystick").GetComponent<Joystick>();
        fireStick = GameObject.Find("R-Joystick").GetComponent<Joystick>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!Mathf.Approximately(moveStick.Horizontal, 0f) ||
                !Mathf.Approximately(moveStick.Vertical, 0f))
            {
            animator.SetBool("isRunning",true);
        }
        else if (!Mathf.Approximately(fireStick.Horizontal, 0f) ||
                !Mathf.Approximately(fireStick.Vertical, 0f))
        {
            animator.SetBool("isShooting", true);
        }

        if (!moveStick)
        {
            Debug.Log("Could not find left joystick in IdleSoldier!");
        }
        if (!fireStick)
        {
            Debug.Log("Could not find right joystick in IdleSoldier!");
        }
    }

}
