﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingSoldier : StateMachineBehaviour
{
    // Reference to the joystick associated with shooting
    Joystick fireStick;

    // Projectile
    GameObject projectile;
    GameObject clone;

    // Normalised time (stateInfo) that the last particle was emitted
    float prevFireTime;

    Vector3 cameraTransformedInputDirection;
    InputToCameraSpace cameraTransformScriptReference;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        fireStick = GameObject.Find("R-Joystick").GetComponent<Joystick>();
        projectile = GameObject.Find("Projectile");

        cameraTransformScriptReference = animator.GetComponent<InputToCameraSpace>();

        // Fire projectile
        clone = Instantiate(projectile, animator.gameObject.transform);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        // Check fire input
        if (Mathf.Approximately(fireStick.Direction.x,0f) &&
            Mathf.Approximately(fireStick.Direction.y, 0f))
        {
            animator.SetBool("isShooting", false);
        }
        else
        {
            // Change the player characters direction
            cameraTransformedInputDirection = cameraTransformScriptReference.getInputToCameraSpace(fireStick.Direction.x, fireStick.Direction.y);

            animator.gameObject.transform.rotation = Quaternion.LookRotation(cameraTransformedInputDirection, Vector3.up);
            animator.SetBool("isShooting", true);
        }

        // Check at least one cycle has passed.
        if (stateInfo.normalizedTime >= prevFireTime+1 )
        {
            prevFireTime = stateInfo.normalizedTime;
            clone = Instantiate(projectile, animator.gameObject.transform);
        }

        // Error logging
        if (!fireStick)
        {
            Debug.Log("Shooting stick not found in scene!");
        }

        if(!projectile)
        {
            Debug.Log("Can't find projectile in Shooting Soldier!");
        }

        if (!cameraTransformScriptReference)
        {
            Debug.Log("Couldn't find the input transformation script in Shooting!");
        }

    } // OnStateUpdate

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Ensure that cycle fire time is reset so that if state is re-entered, 
        // projectiles will still appear at loop end on holding shoot
        prevFireTime = 0f;
    }

}
