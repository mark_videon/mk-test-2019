﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    Uses lookAt and Lerp to have the gameobject follow and face a target gameobject.
*/
public class CameraControl : MonoBehaviour
{
    [SerializeField] Transform target;

    Vector3 distanceAway = new Vector3(-4f ,12f, -4f);
    Vector3 lookVector = new Vector3();

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(target);
        this.transform.position = Vector3.Lerp(this.transform.position, target.position + distanceAway, 10f * Time.deltaTime);
    }

    // Outside functions can update the look
    public void UpdateLook(Vector3 input)
    {
        lookVector.x = input.x;
        lookVector.y = input.y;
        lookVector.z = input.z;
    }
}
