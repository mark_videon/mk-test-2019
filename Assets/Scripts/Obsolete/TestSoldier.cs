﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSoldier : MonoBehaviour
{
    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        // Running
        if (Input.GetKey(KeyCode.W))
        {
            animator.SetTrigger("isRunning");
        } 

        // Shooting
        if (Input.GetKey(KeyCode.Space))
        {
            animator.SetTrigger("isShooting");
        }
    }
}
