﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    float lifetime = 3f;
    float speed = 5f;


    Vector3 faceDirection = new Vector3();

    // Start is called before the first frame update
    void Start()
    {
        if (this.gameObject.name == "Projectile")
        {
            Debug.Log("Please note, there is a GO named 'Projectile' in the " +
                      "scene with its mesh renderer deactivated.");
        }
        else
        {
            this.GetComponent<MeshRenderer>().enabled = true;
            Invoke("DestroyProjectile", lifetime);


            // Relies on accurate alignment between the player model and the
            // cloned Gameobject in the scene
            faceDirection = this.transform.position - this.transform.parent.transform.position;
            faceDirection.y = 0f;

            // Decouple from parent so that if the parent transform is modified,
            // projectile is unaffected
            this.transform.parent = null;
        }

    }

    // Update is called once per frame
    void Update()
    {
        // Check if the attached is not the original projectile
        if (this.gameObject.name != "Projectile")
        {
            this.transform.position += faceDirection * Time.deltaTime * speed;
        }
    }

    void DestroyProjectile()
    {
        Destroy(this.gameObject);
    }
}
